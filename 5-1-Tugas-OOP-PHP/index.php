<?php
abstract class Fight {
    public $attackPower;
    public $defencePower;

    abstract public function serang($lawan);
    abstract public function diserang($lawan);
}

abstract class Hewan extends Fight {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    abstract public function atraksi();
    abstract public function getInfoHewan();
}

// -------- Class Elang ------------
class Elang extends Hewan {
    protected $jumlahKaki =2;
    protected $keahlian = 'terbang tinggi';
    protected $attackPower = 10;
    protected $defencePower = 5;

    public function __construct($nama)
    {
        $this->nama = $nama;
    }

    public function atraksi()
    {
        echo $this->nama . "sedang" . $this->keahlian;
    }

    public function diserang($lawan)
    {
        $this->darah - ($lawan->attackPower / $this->defencePower);
        echo $lawan . " sedang diserang";
    }

    public function serang($lawan)
    {
        $this->diserang($lawan);
        echo $this->nama . " sedang menyerang " . $lawan;
    }


    public function getInfoHewan()
    {
        echo $this->nama . ",Darah = " . $this->darah . ', Attack power = ' . $this->attackPower . ", Defence power = " . $this->defencePower . ", Keahlian = " . $this->keahlian;
    }
}

// -------- Class Harimau ------------
class Harimau extends Hewan {
    protected $jumlahKaki = 4;
    protected $keahlian = "lari cepat";
    protected $attackPower = 7;
    protected $defencePower = 8;

    public function __construct($nama)
    {
        $this->nama = $nama;
    }

    public function atraksi()
    {
        echo $this->nama . "sedang" . $this->keahlian;
    }

    public function diserang($lawan)
    {
        $this->darah - ($lawan->attackPower / $this->defencePower);
        echo $lawan . " sedang diserang";
    }

    public function serang($lawan)
    {
        $this->diserang($lawan);
        echo $this->nama . " sedang menyerang " . $lawan;
    }


    public function getInfoHewan()
    {
        echo $this->nama . ",Darah = " . $this->darah . ', Attack power = ' . $this->attackPower . ", Defence power = " . $this->defencePower . ", Keahlian = " . $this->keahlian;
    }
}



?>