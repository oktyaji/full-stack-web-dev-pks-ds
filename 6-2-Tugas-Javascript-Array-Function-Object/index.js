// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort().forEach(function(hewan) {
    console.log(hewan)
})


// Soal 2
function introduce(data) {
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + "Jalan Pelesiran, dan saya punya hobby yaitu " + data.hobby
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"


// Soal 3
function hitung_huruf_vokal(string) {
    var arrString = string.toLowerCase().split("");
    var count = arrString.filter(function (kata) {
        return kata == "a" | kata == "i" | kata == "u" | kata == "e" | kata == "o"
    }).length
    return count
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


// Soal 4
function hitung(number) {
    var last = -2;
    for (let i = -2; i < number * 2; i+=2) {
        last = i
    }
    return last
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8