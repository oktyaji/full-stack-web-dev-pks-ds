// Soal 1
const luas = (panjang, lebar) => {
    return panjang * lebar
}

const keliling = (panjang, lebar) => {
    return 2 * (panjang + lebar)
}
//Driver Code
console.log(luas(12, 8))
console.log(keliling(12, 8))


// Soal 2
const newFunction = function literal(firstName, lastName){
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
        console.log(`${firstName} ${lastName}`)
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()


// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

let { firstName, lastName, address, hobby } = newObject

console.log(firstName, lastName, address, hobby)



// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


// Soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
var after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`
// Driver Code
console.log(before)
console.log(after)