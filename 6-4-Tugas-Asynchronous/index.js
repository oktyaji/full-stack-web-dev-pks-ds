var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// Tulis code untuk memanggil function readBooks di sini
let time = 10000
readBooks(time, books[0], (sisaWaktu) => {
    time = sisaWaktu
    readBooks(time, books[1], (sisaWaktu) => {
        time = sisaWaktu
        readBooks(time, books[2], (sisaWaktu) => {
            time = sisaWaktu
            readBooks(time, books[3], (sisaWaktu) => {
                time = sisaWaktu
            })
        })
    })
})

