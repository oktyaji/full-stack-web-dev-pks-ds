var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
let time = 10000
readBooksPromise(time, books[0])
.then((result) => {
    readBooksPromise(result, books[1])
    .then(result => {
        readBooksPromise(result, books[2])
        .then(result => {
            readBooksPromise(result, books[3])
        })
        .catch(res => {
            console.log(`saya kekurangan waktu membaca ${-1 * res / 1000} detik`)
        })
    })
    .catch(res => {
        console.log(`saya kekurangan waktu membaca ${-1 * res / 1000} detik`)
    })
})
.catch(res => {
    console.log(`saya kekurangan waktu membaca ${-1 * res / 1000} detik`)
})

