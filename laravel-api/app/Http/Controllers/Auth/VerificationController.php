<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'otp' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        if (!$otp_code) {
            return response()->json([
                'success' => false,
                'message' => 'Otp tidak terbaca'
            ], 400);
        }

        $now = Carbon::now();

        if ($now > $otp_code->valid_until) {
            return response()->json([
                'success' => false,
                'message' => 'Otp kadaluwarsa'
            ], 400);
        }

        $user = User::where('id', $otp_code->user_id)->first();
        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();
        return response()->json([
            'success' => true,
            'message' => 'User telah terverifikasi',
            'data' => $user
        ], 400);

    }
}
