<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStored;
use App\Mail\CommentAuthorMail;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
    * index
    *
    * @return void
    */
    public function index(Request $request) {
        $post_id = $request->post_id;
        //get data from table comments
        $comments = Comment::where('post_id', $post_id)->latest()->get();

        // make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comments
        ], 200);
    }

    /**
    * show
    *
    * @param  mixed $id
    * @return void
    */
    public function show($id)
    {
        //find post by ID
        $comment = Comment::find($id);

        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Comment',
                'data'    => $comment
            ], 200);
        }

        //make response JSON
        return response()->json([
            'success' => false,
            'message' => 'Detail Data Comment Not Found'
        ], 404);
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id
        ]);

        // memanggil event CommentStored
        event(new CommentStored($comment));

        // ini dikirim kepada yang memiliki post
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        // ini dikirim kepada yang memiliki comment
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        //success save to database
        if($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Comment $comment)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comment by ID
        $comment = Comment::findOrFail($comment->id);

        if($comment) {

            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Post tidak tersedia di User'
                ], 403);
            }
            //update $comment
            $comment->update([
                'content'     => $request->content
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find comment by ID
        $comment = Comment::findOrfail($id);

        if($comment) {
            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Post tidak tersedia di User'
                ], 403);
            }

            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);
        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
