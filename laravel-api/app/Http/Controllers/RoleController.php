<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
    * index
    *
    * @return void
    */
    public function index() {
        //get data from table roles
        $roles = Role::latest()->get();

        // make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Roles',
            'data'    => $roles
        ], 200);
    }

    /**
    * show
    *
    * @param  mixed $id
    * @return void
    */
    public function show($id)
    {
        //find role by ID
        $roles = Role::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $roles
        ], 200);
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $roles = Role::create([
            'name' => $request->name
        ]);

        //success save to database
        if($roles) {
            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $roles
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 409);
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Role $roles)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find roles by ID
        $roles = Role::findOrFail($roles->id);

        if($roles) {
            //update $roles
            $roles->update([
                'name' => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data'    => $roles
            ], 200);

        }

        //data role not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find roles by ID
        $roles = Role::findOrfail($id);

        if($roles) {

            //delete comment
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);
        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
