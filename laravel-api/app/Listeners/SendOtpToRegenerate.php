<?php

namespace App\Listeners;

use App\Events\Regenerate;
use App\Mail\RegenerateMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOtpToRegenerate implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Regenerate  $event
     * @return void
     */
    public function handle(Regenerate $event)
    {
        Mail::to($event->user->email)->send(new RegenerateMail($event->otp_code));
    }
}
